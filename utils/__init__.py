import ctypes
import ctypes.wintypes
import ipaddress
import os
import ping3
import socket
import sys
import time

from colorama import Fore, Style, init

init(convert=True)


class Console:
    @staticmethod
    def error(message):
        print(f"[{Fore.LIGHTRED_EX}ERROR{Style.RESET_ALL}] {message}")

    @staticmethod
    def success(message):
        print(f"[{Fore.LIGHTGREEN_EX}SUCCESS{Style.RESET_ALL}] {message}")

    @staticmethod
    def info(message):
        print(f"\n[{Fore.LIGHTBLUE_EX}INFO{Style.RESET_ALL}] {message}")

    @staticmethod
    def warning(message):
        print(f"[{Fore.YELLOW}WARNING{Style.RESET_ALL}] {message}")

    @staticmethod
    def clear():
        os.system('cls' if os.name == 'nt' else 'clear')


class TitleBuilder:
    @staticmethod
    def title(titulo):
        size = 80
        line = "=" * size
        formatted_title = titulo.center(size - 2)
        print("\n" + line)
        print(f"|{Fore.YELLOW}{formatted_title}{Fore.YELLOW}{Style.RESET_ALL}|")
        print(line)

    @staticmethod
    def format_menu(title_text, options):
        print("\n")
        print("=" * 80)
        print(f"|{title_text:^78}|")
        print("=" * 80)
        for idx, option in enumerate(options, start=1):
            print(f"{idx}. {option}")
        print()


class NetworkUtils:
    @staticmethod
    def get_ip():
        hostname = socket.gethostname()
        ip_address = socket.gethostbyname(hostname)
        return ip_address

    @staticmethod
    def get_hostname():
        hostname = socket.gethostname()
        return hostname

    @staticmethod
    def get_fqdn():
        fqdn = socket.getfqdn()
        return fqdn

    @staticmethod
    def get_all():
        hostname = socket.gethostname()
        ip_address = socket.gethostbyname(hostname)
        fqdn = socket.getfqdn()
        return hostname, ip_address, fqdn

    @staticmethod
    def test_ip(ip):
        try:
            ipaddress.ip_address(ip)
            return True
        except ValueError:
            return False

    @staticmethod
    def ping(ip):
        return_ping = ping3.ping(ip, timeout=4)
        if return_ping:
            return True
        else:
            return False


class WinUtils:
    def __init__(self, log):
        self.log = log

    def check_admin(self):
        try:
            is_admin = ctypes.windll.shell32.IsUserAnAdmin()

            if not is_admin:
                raise PermissionError("Programa não executado como administrador.")

        except PermissionError as e:
            Console.error(f"O Diagnóstico deve ser executado como administrador.")
            self.log.error(f"{str(e)}")
            input("Pressione qualquer tecla para sair...")
            sys.exit(1)
        except Exception as e:
            Console.error(f"Ocorreu um erro ao verificar permissões de administrador.")
            self.log.exception(f"Ocorreu um erro ao verificar se o usuário é administrador: {str(e)}")
            input("Pressione qualquer tecla para sair...")
            sys.exit(1)

    @staticmethod
    def clean_rc_path(rc_path):
        def remove_png_files_in_directory(directory):
            try:
                file_list = os.listdir(directory)
                for file_name in file_list:
                    file_path = os.path.join(directory, file_name)
                    if os.path.isfile(file_path) and file_name.endswith('.png'):
                        os.remove(file_path)
                        Console.info("Limpeza de arquivos temporários concluída.")
            except Exception as e:
                Console.error(f"Erro ao limpar arquivos temporários: {str(e)}")

        remove_png_files_in_directory(rc_path)
