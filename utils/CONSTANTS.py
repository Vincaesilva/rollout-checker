path_hosts_file = "c:\\Windows\\System32\\drivers\\etc\\hosts"
# path_hosts_file = "c:\\dev\\hosts.txt"
path_tcagent_folder = 'C:\\Tc-Agent'
log_path = "c:\\RolloutChecker"
hml_tcagent_controlled_url = "http://10.1.50.119:7010/layout/receita-carimbo-epson"
hml_tcagent_ofex_url = "http://10.1.50.119:7010/layout/impressao-ofertas-exclusivas/client?bandeira=DROGASIL"
local_tcprinter_url = "http://localhost:8092/api/printers/print"
hosts_ports_agent = [("tcprinterservice", 8092), ("tcscannerservice", 8093)]


usb_printer_id = {
    "USB\\VID_04B8&PID_0202\\58324D510": "EPSON TM-T88V Receipt",
    "USB\\VID_04B8&PID_0202\\54504C460": "EPSON TM-L90 Label",
    "USB\\VID_04B8&PID_0202\\584159590": "EPSON TM-L90 Label",
    # "USB\\VID_0424&PID_5744\\5&320AAD6": "EPSON TM-L90 Label",
    }

prefix_printer_name = 'TM-'
tcagent_header = {
    "ofex": {
        "accept": "image/png",
        "X-PRINT-DIMENSION": '{"width": 1200, "height": 1200}',
        "Content-Type": "application/json"
        },
    "controlled": {
        "accept": "image/png",
        "X-COD-FILIAL": "16",
        "X-PRINT-DIMENSION": '{"width": 600, "height": 600}',
        "Content-Type": "application/json"
        }
    }

tcagent_data = {
    "ofex": {
        "canal": 9,
        "cdFilial": 1047,
        "cdOperador": 6000,
        "idCliente": 76092141
        },
    "controlled": [
        "TESTE AUTOMATIZADO TI",
        "33116038",
        "Rua Corifeu de Azevedo Marques",
        "251",
        "VILA SUZANA",
        "SAO PAULO",
        "SP",
        "05630020",
        "983537167",
        "Avenida Corifeu de Azevedo Marques",
        "3097",
        "61585865293400",
        "7896212420117<C>*CELECOX 200 TEG 10-C1<C>1037005980091<C>00050<C>10/2023<C>5<C>7896212420117<C><L><P>7896112164838<C>*CELECOX 200 TEG "
        "10-C1<C>1037005980091<C>00050<C>10/2023<C>5<C>7896112164838<C><L>",
        "464",
        "AP. 24B",
        "00",
        "00000000\\r\\n",
        "SSP",
        "21974903893",
        "350865573",
        "3935",
        "1234567",
        "15/02/1984",
        "SP",
        "Joaquim teste",
        "CRO",
        "",
        "",
        "",
        "",
        "18/07/2022",
        "",
        "2",
        "SP",
        "false"
        ]
    }


tcprinter_header = {
    "ofex": {
        'accept': 'application/json;charset=UTF-8',
        'x-printer-attrb': '{"x": 0, "y": 0, "w": 300, "h": 2970, "orientation": 0, "fit": true}'
        },
    "controlled": {
        'accept': 'application/json;charset=UTF-8',
        'x-printer-attrb': '{"x": 0, "y": 0, "w": 300, "h": 320, "orientation": 0, "fit": true}'
        }
    }

