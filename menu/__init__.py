import sys

import utils
from services import printers
from services.hosts import HostsServices
from services.printers import PrinterServices
from services.printers.agent import AgentServices
from utils import TitleBuilder as Title, Console


class MainMenu:
    def __init__(self, logger):
        self.log = logger
        self.menu_options = {
            "1": {"title": "MENU DE HOSTS-DNS", "class": HostsSubmenu},
            "2": {"title": "MENU DE IMPRESSORAS", "class": PrintersSubmenu},
            # "3": {"title": "MENU TC AGENT", "class": TCAgentSubmenu},
            "3": {"title": "Sair", "func": self.exit}
            }
        self.hosts_services = HostsServices(self.log)
        self.printer_services = PrinterServices(self.log)
        self.agent_services = AgentServices(self.log)

    @staticmethod
    def display_menu_options():
        Title.format_menu("MENU PRINCIPAL", [
            "Configurações de Hosts",
            "Configurações de Impressoras",
            # "Configurações de TC-Agent",
            "Sair"
            ])

    @staticmethod
    def exit():
        sys.exit(1)

    def main(self):
        Title.title("INITIALIZING ROLLOUT DIAGNOSTIC")
        self.hosts_services.autostart()
        self.printer_services.autostart()
        self.agent_services.autostart()
        while True:
            self.display_menu_options()
            choice = input(f"\nDigite o número da opção desejada: ")

            selected_option = self.menu_options.get(choice)
            if selected_option:
                func = selected_option.get("func")
                if func:
                    func()  # Call the exit function if selected_option is 4
                else:
                    submenu_class = selected_option["class"]
                    if submenu_class:
                        submenu_instance = submenu_class(self.log, self.printer_services)
                        result = submenu_instance.display()
                        if result == "back_to_main_menu":
                            continue
            else:
                Console.error("Opção inválida. Por favor, escolha uma opção válida.\n")


class Submenu:
    def __init__(self, title, options):
        self.title = title
        self.options = options

    def display(self):
        while True:
            Title.format_menu(self.title, [f"{option['name']}" for key, option in self.options.items()])
            choice = input(f"\nDigite o número da opção desejada: ")

            if choice in self.options:
                selected_option = self.options[choice]
                Console.clear()
                result = selected_option["function"]()
                if result == "back_to_main_menu":
                    return result
            else:
                Console.error("Opção inválida. Por favor, escolha uma opção válida.\n")


class HostsSubmenu(Submenu):
    def __init__(self, log, printer_services):
        self.hosts = HostsServices(log)
        self.printer = printer_services
        title = "MENU DE HOSTS-DNS"
        options = {
            "1": {"name": "Obter configurações atuais", "function": self.open_current_settings},
            "2": {"name": "Configurar hosts", "function": self.configure_hosts},
            "3": {"name": "Voltar ao menu principal", "function": self.back_to_main_menu}
            }
        super().__init__(title, options)

    def open_current_settings(self):
        self.hosts.list_services()

    def configure_hosts(self):
        self.hosts.edit_services(self.printer.printer_found)

    @staticmethod
    def back_to_main_menu():
        print("Voltando ao menu principal.")
        return "back_to_main_menu"


class PrintersSubmenu(Submenu):
    def __init__(self, log, printer_services):
        self.printer = printer_services
        title = "MENU DE IMPRESSORAS"
        options = {
            "1": {"name": "Abrir configurações de impressão", "function": self.printer.func_menu["open_painel"]},
            "2": {"name": "Buscar impressoras USB", "function": self.printer.func_menu["get_usb"]},
            "3": {"name": "Buscar Impressoras na rede", "function": self.printer.func_menu["get_eth"]},
            "4": {"name": "Enviar impressão teste - TM-T88V", "function": lambda param=self.printer.func_menu["test_ofex"]: param(1)},
            "5": {"name": "Enviar impressão teste - TM-L90", "function": lambda param=self.printer.func_menu["test_ofex"]: param(2)},
            "6": {"name": "Realizar diagnostico das impressoras", "function": self.printer.func_menu["extract_info"]},
            "7": {"name": "Voltar ao menu principal", "function": lambda: "back_to_main_menu"}
            }
        super().__init__(title, options)


class TCAgentSubmenu(Submenu):
    def __init__(self, log, printer_services):
        title = "MENU TC AGENT"
        options = {
            "1": {"name": "Abrir configurações de tc-agent", "function": self.abrir_configuracoes_tc_agent},
            "2": {"name": "Reiniciar TC Agent", "function": self.restart_agent},
            "3": {"name": "Voltar ao menu principal", "function": self.back_to_main_menu},
            }
        super().__init__(title, options)

    @staticmethod
    def abrir_configuracoes_tc_agent():
        print("Abrindo configurações de tc-agent.")

    @staticmethod
    def restart_agent():
        print("Reiniciando o TC Agent.")

    @staticmethod
    def back_to_main_menu():
        print("Voltando ao menu principal.")
        return "back_to_main_menu"


def main(logger):
    main_menu = MainMenu(logger)
    main_menu.main()
