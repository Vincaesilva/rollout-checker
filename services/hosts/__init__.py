import time

from colorama import Fore, Style

from utils import Console, CONSTANTS, NetworkUtils


class HostsServices:
    def __init__(self, log):
        self.log = log
        self.hosts_file_path = CONSTANTS.path_hosts_file

    def autostart(self):
        self.list_services()

    def list_services(self):

        services = ["tcprinterservice", "tcscannerservice", "tcbiometriaservice", "tccontroladosprinterservice"]
        services_found = set()
        not_found = []

        try:
            with open(self.hosts_file_path, "r") as file:
                lines = file.readlines()
                print(f"\nCONFIGURAÇÕES ATUAIS DO ARQUIVO HOSTS:\n")
                for line in lines:
                    for service in services:
                        if service in line:
                            print(line.strip())
                            services_found.add(service)
                            break

                for service in services:
                    if service not in services_found:
                        if service not in not_found:
                            not_found.append(service)

                if not_found:
                    print("\n")
                    Console.warning(f"Os seguintes serviços não foram encontrados: {Fore.RED}{', '.join(not_found)}{Style.RESET_ALL}")
                    self.log.warning(
                        f"Os seguintes serviços não foram encontrados no arquivo hosts: {', '.join(not_found)}")

        except FileNotFoundError:
            Console.error("Arquivo hosts não encontrado.")
            self.log.error("Arquivo hosts não encontrado.")
        except PermissionError:
            Console.error(
                f"O arquivo '{self.hosts_file_path}' não tem permissão de leitura, verifique as permissões do arquivo.")
            self.log.error(f"O arquivo '{self.hosts_file_path}' não tem permissão de leitura.")
        except Exception as e:
            Console.error(f"Ocorreu um erro ao ler o arquivo hosts: {str(e)}")
            self.log.exception(f"Ocorreu um erro ao ler o arquivo hosts: {str(e)}")
        finally:
            services_found.clear()
            not_found.clear()

    def edit_services(self, has_usb_printer_connected):
        is_set_automatic_ip = []
        localhost = {
            "EPSON TM-T88V Receipt": "127.0.0.1",
            "EPSON TM-L90 Label": "127.0.0.1"
            }

        T88V = list(localhost.keys())[0]
        L90 = list(localhost.keys())[1]

        def get_valid_ip_input(machine_name):
            while True:
                ip_input = input(f"\nInsira o IP da máquina ({machine_name}): ")
                if NetworkUtils.test_ip(ip_input):
                    return ip_input
                else:
                    Console.error(f"O IP {ip_input}({machine_name}) não é válido. Por favor, insira um IP válido.")

        if T88V not in has_usb_printer_connected.get("USB", []):
            tcprinterservice_ip = get_valid_ip_input(T88V)
        else:
            tcprinterservice_ip = localhost[T88V]
            is_set_automatic_ip.append("TC Printer Service (TM-T88V)")

        if L90 not in has_usb_printer_connected.get("USB", []):
            tccontroladosprinterservice_ip = get_valid_ip_input(L90)
        else:
            tccontroladosprinterservice_ip = localhost[L90]
            is_set_automatic_ip.append("TC Controlados Printer Service(TM-L90)")

        new_hosts_content = f"########## CONFIGS TC SEM APPLET ##########\n\n"
        new_hosts_content += f"{tcprinterservice_ip}\ttcprinterservice\n"
        new_hosts_content += "127.0.0.1\ttcscannerservice\n"
        new_hosts_content += "127.0.0.1\ttcbiometriaservice\n"
        new_hosts_content += f"{tccontroladosprinterservice_ip}\ttccontroladosprinterservice\n\n"
        new_hosts_content += f"######## FIM CONFIGS TC SEM APPLET ########"

        try:
            with open(self.hosts_file_path, "r") as hosts_file:
                existing_content = hosts_file.read()
        except FileNotFoundError:
            Console.error("Arquivo hosts não encontrado.")
            return

        separator_options = ["########## CONFIGS TC SEM APPLET ##########", "#TC-Agent Local"]
        separator = None

        for option in separator_options:
            if option in existing_content:
                separator = option
                break

        if separator is not None:
            content_above_separator, _ = existing_content.split(separator, 1)
        else:
            content_above_separator = existing_content

        new_content = content_above_separator + new_hosts_content

        try:
            with open(self.hosts_file_path, "w") as hosts_file:
                hosts_file.write(new_content)
            if is_set_automatic_ip:
                Console.info(
                    f"Impressoras USBs foram detectadas na maquina, os serviços \n{Fore.GREEN}{', '.join(is_set_automatic_ip)} "
                    f"{Style.RESET_ALL}foram "
                    f"configurados com IP local.")
                self.log.info(
                    f"[USB Printer Found] Hosts configurados com IP local para os serviços: {', '.join(is_set_automatic_ip)}")
            Console.success("Arquivo hosts foi atualizado com sucesso!")

        except PermissionError:
            Console.error(f"O arquivo '{self.hosts_file_path}' não tem permissão de leitura, verifique as permissões do arquivo.")
        except Exception as e:
            Console.error(f"Erro ao escrever no arquivo hosts: {str(e)}")
