import logging
import os
import atexit

import utils
from utils.CONSTANTS import log_path


class Logger:
    def __init__(self):
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        log_file = "RolloutChecker.log"
        self.log_file = os.path.join(log_path, log_file)

        # Create a logger instance
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        formatter = logging.Formatter('%(asctime)s - [%(levelname)s] %(message)s', datefmt='%d/%m/%Y %H:%M:%S')

        file_handler = logging.FileHandler(self.log_file, encoding='windows-1252')
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

        atexit.register(self.exit_program)

    def start_program(self):
        self.logger.info(f"{'=' * 50}")
        self.logger.info("Iniciando diagnóstico do rollout...")
        self.logger.info(f"{'=' * 50}")

    def exit_program(self):
        utils.WinUtils.clean_rc_path(log_path)
        self.logger.info(f"{'=' * 50}")
        self.logger.info("Finalizando diagnóstico do rollout...")
        self.logger.info(f"{'=' * 50}\n\n")

    def info(self, message):
        self.logger.info(message)

    def warning(self, message):
        self.logger.warning(message)

    def error(self, message):
        self.logger.error(message)

    def exception(self, message):
        self.logger.exception(message)

    def critical(self, message):
        self.logger.critical(message)

    def debug(self, message):
        self.logger.debug(message)
