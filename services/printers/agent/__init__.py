import os
import socket
import zipfile

from colorama import Fore, Style

from utils import Console, CONSTANTS


class AgentServices:
    def __init__(self, log):
        self.log = log
        self.hosts_ports = CONSTANTS.hosts_ports_agent
        self.path_tc_agent = CONSTANTS.path_tcagent_folder

    def autostart(self):
        self.check_jar_version()
        self.run_services_check()

    def run_services_check(self):
        Console.info("Verificando se os serviços do agente estão rodando...")
        for host, port in self.hosts_ports:
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    result = s.connect_ex((host, port))
                    if result == 0:
                        Console.success(f"{host} está online!")
                        self.log.info(f"{host} está online!")
                    else:
                        Console.warning(f"{host} está offline!")
                        self.log.warning(f"{host} está offline!")
            except (socket.gaierror, socket.error) as e:
                error_message = f"Não foi possível conectar-se ao host {host} na porta {port}."
                if isinstance(e, socket.gaierror):
                    error_message = f"O Host {host} não pode ser resolvido, verifique o arquivo hosts."
                Console.error(error_message)
                self.log.error(error_message)

    def check_jar_version(self, ext='.jar'):
        lib_agent_path = os.path.join(self.path_tc_agent, 'lib')
        if os.path.exists(lib_agent_path):
            files = os.listdir(lib_agent_path)
            versions = []

            Console.info("Verificando as versões dos arquivos jar...")
            required_jars = {'tc-printer.jar', 'tc-scanner.jar'}

            for required_jar in required_jars:
                if required_jar not in files:
                    Console.error(f"O arquivo {required_jar} não foi encontrado no diretório {lib_agent_path}.")
                    self.log.error(f"O arquivo {required_jar} não foi encontrado na máquina {os.environ['COMPUTERNAME']}.")

            for file in files:
                if file.lower().endswith(ext):
                    path_file = os.path.join(lib_agent_path, file)

                    try:
                        with zipfile.ZipFile(path_file, 'r') as jar_file:
                            version_info = jar_file.read('META-INF/MANIFEST.MF').decode("utf-8")
                            version_line = [line for line in version_info.split('\n') if 'Implementation-Version' in line][
                                0]
                            version = version_line.split(':')[1].strip()
                            versions.append(f"{Fore.GREEN}{file}: {version} {Style.RESET_ALL}")
                    except Exception as e:
                        Console.error(f"Erro ao obter a versão do arquivo {file}: {e}")
                        self.log.exception(f"Erro ao obter a versão do arquivo {file}: {e}")

            if versions:
                for version in versions:
                    print(version)
            else:
                Console.warning(f"Nenhum arquivo .jar encontrado.")
                self.log.warning(f"Nenhum arquivo .jar encontrado na máquina {os.environ['COMPUTERNAME']}.")

        else:
            Console.error(f"O diretório {lib_agent_path} não foi encontrado.")
            self.log.error(f"O diretório {lib_agent_path} não foi encontrado na máquina {os.environ['COMPUTERNAME']}.")