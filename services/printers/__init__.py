import pickle
import subprocess
import time

import requests
import os
import win32com.client
import json

import win32print

from utils import Console, NetworkUtils, CONSTANTS
from colorama import Fore, Style


class PrinterServices:
    def __init__(self, log):
        self.log = log
        self.file_path = os.environ.get("TEMP")
        self.device_name_map = CONSTANTS.usb_printer_id
        self.prefix_printer_name = CONSTANTS.prefix_printer_name

        self.tcagent_controlled_url = CONSTANTS.hml_tcagent_controlled_url
        self.tcagent_ofex_url = CONSTANTS.hml_tcagent_ofex_url

        self.tcagent_header = CONSTANTS.tcagent_header
        self.tcagent_data = CONSTANTS.tcagent_data

        self.tcprinter_url = CONSTANTS.local_tcprinter_url
        self.tcprinter_header = CONSTANTS.tcprinter_header

        self.printer_found = {
            "USB": [],
            "ETH": []
            }

        self.func_menu = {
            "open_painel": self.open_control_printers,
            "get_usb": self.check_usb_printer,
            "get_eth": self.check_eth_printer,
            "test_ofex": self.test_printer_label,
            "test_controlled": self.test_printer_label,
            "extract_info": self.extract_print_info

            }

        self.printer_info_list = {}
        self.printer_inconsistencies = {}

    def autostart(self):
        self.check_usb_printer()
        self.check_eth_printer()
        self.extract_print_info()

    def check_usb_printer(self):
        mapped_printer_deviceid = list(self.device_name_map.keys())
        devices_found = []

        try:
            wmi = win32com.client.GetObject("winmgmts:")
            query = f"SELECT * FROM Win32_PnPEntity WHERE PNPClass = 'USB'"
            devices = wmi.ExecQuery(query)

            Console.info("Buscando impressoras USB...")
            for printer in mapped_printer_deviceid:
                for device in devices:
                    if printer in device.DeviceID:
                        printer_name = self.device_name_map.get(printer, "Desconhecido")
                        devices_found.append(printer_name)
                        self.printer_found["USB"].append(printer_name)

            if devices_found:
                devices_found_str = ", ".join(devices_found)

                Console.success(f"Impressoras conectadas via USB na máquina: {Fore.GREEN}{devices_found_str}{Style.RESET_ALL}")
                self.log.info(f"Impressoras conectadas via USB: {devices_found_str}")
            else:
                Console.warning("Nenhuma impressora USB conectada.")
                self.log.info(f"Não foram encontradas impressoras conectadas via USB na máquina {os.environ['COMPUTERNAME']}.")
        except Exception as e:
            Console.error(f"Erro ao buscar impressoras USB: {str(e)}")
            self.log.error(f"Erro ao buscar impressoras USB: {str(e)}")

    def check_eth_printer(self, verbose=True):
        def log_message(message, level="info"):
            if verbose:
                if level == "info":
                    Console.info(message)
                elif level == "success":
                    Console.success(message)
                elif level == "warning":
                    Console.warning(message)
                elif level == "error":
                    Console.error(message)

        local_ip = NetworkUtils.get_ip()
        printers_found = []

        if not local_ip:
            log_message("Não foi possível obter o endereço IP local.", level="error")
            self.log.error("Não foi possível obter o endereço IP local.")
            return printers_found

        subnet_prefix = ".".join(local_ip.split(".")[:-1])
        printer_names = ["EPSON TM-T88V Receipt", "EPSON TM-L90 Label"]

        for printer_name in printer_names:
            log_message(f"Buscando {printer_name} na rede...", level="info")
            ip_range = [61] if printer_name == "EPSON TM-T88V Receipt" else [62]

            for i in ip_range:
                target_ip = f"{subnet_prefix}.{i}"
                response_time = NetworkUtils.ping(target_ip)

                if response_time:
                    log_message(f"Impressora {printer_name} encontrada em {target_ip}!", level="success")
                    printers_found.append({printer_name: target_ip})
                    self.printer_found["ETH"].append({printer_name})
                else:
                    log_message(f"Impressora {printer_name} não encontrada em {target_ip}!", level="warning")
        if printers_found:
            log_message(f"Foram encontradas {len(printers_found)} impressoras na rede.", level="info")
            self.log.info(f"Foram encontradas {len(printers_found)} impressoras na rede. {printers_found}")
        else:
            # log_message("Nenhuma impressora encontrada na rede.", level="warning")
            self.log.info("Nenhuma impressora encontrada na rede.")

        return printers_found

    def extract_print_info(self):
        try:
            printers = win32print.EnumPrinters(win32print.PRINTER_ENUM_LOCAL, None, 1)
            for printer in printers:
                printer_name = printer[2]
                if self.prefix_printer_name in printer_name:
                    hprinter = win32print.OpenPrinter(printer_name)
                    extracted_info = win32print.GetPrinter(hprinter, 2)
                    if extracted_info['pDevMode']:
                        parse_core = {}
                        for field in dir(extracted_info['pDevMode']):
                            if not field.startswith("__"):
                                parse_core[field] = getattr(extracted_info['pDevMode'], field)
                        extracted_info['pDevMode'] = parse_core
                    self.printer_info_list[printer_name] = extracted_info
            self.filter_info_out()
        except Exception as e:
            print(f"Erro ao obter informações das impressoras ou escrever no arquivo: {e}")

    def filter_info_out(self):
        for printer_name, printer_info in self.printer_info_list.items():
            filtered_info = {
                "pPrinterName": printer_info.get("pPrinterName"),
                "pPortName": printer_info.get("pPortName"),
                "pDriverName": printer_info.get("pDriverName"),
                "Status": printer_info.get("Status"),
                "Orientation": printer_info.get("pDevMode").get("Orientation") if printer_info.get("pDevMode") else None,
                "PaperSize": printer_info.get("pDevMode").get("PaperSize") if printer_info.get("pDevMode") else None,
                "PaperLength": printer_info.get("pDevMode").get("PaperLength") if printer_info.get("pDevMode") else None,
                "PaperWidth": printer_info.get("pDevMode").get("PaperWidth") if printer_info.get("pDevMode") else None,
                "PrintQuality": printer_info.get("pDevMode").get("PrintQuality") if printer_info.get("pDevMode") else None,
                "cJobs": printer_info.get("cJobs")
                }

            inconsistencies = []

            if filtered_info["pPortName"] is None:
                inconsistencies.append("A porta de impressão não está configurada corretamente.")
            if filtered_info["PaperLength"] != 2970 or filtered_info["PaperWidth"] != 800:
                inconsistencies.append("Tamanho do papel não está configurado como Full-page Label 80 x 297mm.")
            if filtered_info["Orientation"] != 1:
                inconsistencies.append("Orientação da impressão não está configurada como Retrato.")
            if filtered_info["cJobs"] > 3:
                inconsistencies.append("A Fila de impressão reportou congestionamento.")
            if filtered_info["Status"] != 0:
                inconsistencies.append("A impressora reportou indisponibilidade para impressão.")

            if inconsistencies:
                self.printer_inconsistencies[printer_name] = inconsistencies
            else:
                Console.info(f"Realizando Diagnóstico de {printer_name}...")
                Console.success(f"Não foram encontradas inconsistências de layout na impressora {printer_name}.")
                time.sleep(1)
                Console.warning(f"Não foi possível analisar as seguintes abas: {Fore.RED}DEFINIÇÕES DE DOCUMENTO, DEFINIÇÕES MEIO-TO"
                                f"M{Style.RESET_ALL}.")

        if self.printer_inconsistencies:
            for printer_name, inconsistencies in self.printer_inconsistencies.items():
                Console.info(f"Realizando Diagnóstico de {printer_name}...")
                for inconsistency in inconsistencies:
                    Console.warning(f"{inconsistency}")

    def test_printer_label(self, type_test=1):
        type_label_test = type_test == 1 and "CUPOM OFERTAS" or "ETIQUETA CONTROLADOS"
        printer_name = type_test == 1 and "EPSON TM-T88V Receipt" or "EPSON TM-L90 Label"
        url_generate_image = type_test == 1 and self.tcagent_ofex_url or self.tcagent_controlled_url
        headers_generate_image = type_test == 1 and self.tcagent_header["ofex"] or self.tcagent_header["controlled"]
        data_generate_image = type_test == 1 and self.tcagent_data["ofex"] or self.tcagent_data["controlled"]
        file_path = f"{self.file_path}\\test_printer.png"

        Console.info("Enviando solicitação de impressão...")
        response_generate_image = requests.post(url_generate_image, headers=headers_generate_image, json=data_generate_image)

        if response_generate_image.status_code == 200:
            with open(file_path, "wb") as img_file:
                img_file.write(response_generate_image.content)

            url_send_image = self.tcprinter_url
            headers_send_image = type_test == 1 and self.tcprinter_header["ofex"] or self.tcprinter_header["controlled"]
            form_data_send_image = {
                'printer-name': printer_name,
                }

            files_send_image = {'file': ('test_printer.jpg', open(file_path, 'rb'))}
            response_send_image = requests.post(url_send_image, headers=headers_send_image, data=form_data_send_image,
                                                files=files_send_image)

            if response_send_image.status_code == 200:
                Console.success(f"Teste de impressão {type_label_test} realizado com sucesso!")
                self.log.info(f"Teste de impressão {type_label_test} realizado com sucesso!")
            else:
                Console.error(f"Teste de impressão {type_label_test} falhou!")
                self.log.error(f"Teste de impressão {type_label_test} falhou!")
                try:
                    parse_response = json.loads(response_send_image.text).get("message", "Não foi possivel obter detalhes do erro.")
                    Console.error(f"Resposta do servidor: {parse_response}")
                    self.log.error(f"Resposta do servidor: {parse_response}")
                except json.JSONDecodeError as e:
                    Console.error(f"Resposta do servidor: {response_send_image.text}")
                    self.log.error(f"Resposta do servidor: {response_send_image.text}")

        else:
            try:
                Console.error(f"Ocorreu um erro ao gerar a impressão, o TC-AGENT não respondeu corretamente.")
                self.log.error(f"Ocorreu um erro ao gerar a impressão, o TC-AGENT não respondeu corretamente.")
                parse_response = json.loads(response_generate_image.text).get("message", "Não foi possivel obter detalhes do erro.")
                Console.error(f"Resposta do servidor: {parse_response}")
                self.log.error(f"Resposta do servidor: {parse_response}")
            except json.JSONDecodeError as e:
                Console.error(f"Resposta do servidor: {response_generate_image.text}")
                self.log.error(f"Resposta do servidor: {response_generate_image.text}")

    def open_control_printers(self):
        try:
            subprocess.run(["control", "printers"])
        except Exception as e:
            Console.error(f"Erro ao abrir o painel de impressoras: {str(e)}")
            self.log.error(f"Erro ao abrir o painel de impressoras: {str(e)}")
