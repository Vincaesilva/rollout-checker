import ctypes
import sys

import menu
import utils
from services import Logger
from utils import Console

log = Logger()
log.start_program()
ctypes.windll.kernel32.SetConsoleTitleW("Rollout Checker - TC sem Applet")

try:
	menu.main(log)
except Exception as e:
	Console.error(f"Ocorreu um erro ao executar o programa: {str(e)}")
	log.exception(f"Ocorreu um erro ao executar o programa: {str(e)}")
	input("Pressione qualquer tecla para sair...")
	sys.exit(1)
